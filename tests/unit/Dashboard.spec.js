import {mount} from 'avoriaz';
import expect from 'expect';

import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import moxios from 'moxios';

Vue.use(Vuex);
Vue.use(VueRouter);

import Session from '../../src/js/pages/Sessions.vue';
import Dashboard from '../../src/js/pages/Dashboard.vue';

describe('Dashboard', () => {

  let wrapper;
  let actions;
  let store;
  let getters;

  const routes = [
    {
      path: '/sessions',
      name: 'sessions',
      component: Session,
    },
  ];

  const router = new VueRouter({
    routes,
  });

  beforeEach(() => {
    moxios.install();

    getters = {
      state: () => '',
      currentUser: () => {
        return {
          firstName: 'Foo',
          lastName: 'Bar',
        };
      },
    };

    actions = {
      getScannedCards: () => {
        return new Promise((response, resolve) => {

        });
      },
    };

    store = new Vuex.Store({
      actions,
      getters,
    });

    wrapper = mount(Dashboard, {store, router});
  });

  afterEach(() => {
    moxios.uninstall();
  });

  it('gets successful response from API', (done) => {
    moxios.stubRequest('/api/scans.json', {
      status: 200,
      response: [
        {
          'id': 1,
          'time': '4s',
          'memory': '',
          'hex': '',
          'fc': '',
          'data': '',
        },
      ],
    });

    moxios.wait(() => {
      expect(wrapper.data().scans.length).toBe(0);
      done();
    });
  });

  it('has a button', () => {
    expect(wrapper.contains('button')).toBe(true);
  });
});
