import Vue from 'vue';
import VueAxios from 'vue-axios';
import axios from 'axios/index';

import Jwt from './Jwt';

const ApiService = {
  init() {
    Vue.use(VueAxios, axios);
    // Vue.axios.defaults.baseURL = 'https://getpocket.com/api';
    // Vue.axios.defaults.baseURL = 'http://localhost:3001/api';
    Vue.axios.defaults.baseURL = '/api';
  },

  setHeader() {
    Vue.axios.defaults.headers.common['Authorization'] = `Bearer ${Jwt.getToken()}`;
  },

  get(resource, key = '') {
    return new Promise((resolve, reject) => {
      Vue.axios.get(`${resource}/${key}`).then((response) => resolve(response)).catch((error) => reject(error));
    });
  },

  single(resource) {
    return new Promise((resolve, reject) => {
      Vue.axios.get(resource).then((response) => resolve(response)).catch((error) => reject(error));
    });
  },

  post(resource, params) {
    return new Promise((resolve, reject) => {
      Vue.axios.post(`${resource}`, params).then((response) => resolve(response)).catch((error) => reject(error));
    });
  },

  update(resource, key, params) {
    return new Promise((resolve, reject) => {
      Vue.axios.put(`${resource}/${key}`, params).then((response) => resolve(response)).catch((error) => reject(error));
    });
  },

  destroy(resource, key) {
    return new Promise((resolve, reject) => {
      Vue.axios.delete(`${resource}/${key}`).then((response) => resolve(response)).catch((error) => reject(error));
    });
  }
};

export default ApiService;
