const TOKEN_KEY = 'token';

export default {
  getToken() {
    return window.localStorage.getItem(TOKEN_KEY);
  },

  isInitialized() {
    return window.localStorage.getItem('initialized');
  },

  get(key) {
    return window.localStorage.getItem(key);
  },

  save(value) {
    window.localStorage.setItem(TOKEN_KEY, value);
  },

  saveItem(key, value) {
    window.localStorage.setItem(key, value);
  },

  destroy() {
    window.localStorage.removeItem(TOKEN_KEY);
  },

  destroyItem(key) {
    window.localStorage.removeItem(key);
  },

  destroyAll() {
    window.localStorage.clear();
  }
};
