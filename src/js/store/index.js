import Vue from 'vue';
import Vuex from 'vuex';
import * as getters from './getters';
import app from './modules/app';
import auth from './modules/auth';
import sessions from './modules/sessions';

Vue.use(Vuex);

const store = new Vuex.Store({
  getters,
  modules: {
    app,
    auth,
    sessions
  }
});

export default store;
