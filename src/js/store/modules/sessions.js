import {
  CREATE_SESSION,
  GET_CURRENT_SESSION,
  CURRENT_SESSION_CHANGED,
  GET_PREVIOUS_SESSIONS,
  DELETE_SESSION,
  EDIT_SESSION,
  EXPORT_SESSION,
  EXPORT_SESSIONS
} from '../mutation-types';
import ApiService from '../../services/Api';

const state = {
  currentSession: null
};

const mutations = {
  [CURRENT_SESSION_CHANGED](state, config) {
    state.currentSession = config;
  }
};

const getters = {
  [GET_CURRENT_SESSION](state) {
    return state.currentSession;
  }
};

const actions = {
  [CREATE_SESSION]({ commit }, data) {
    ApiService.setHeader();
    return ApiService.post('control/createsession', {
      sessionname: data
    });
  },
  [EDIT_SESSION]({ commit }, data) {
    ApiService.setHeader();
    return ApiService.post('control/updatesession', data);
  },
  [GET_PREVIOUS_SESSIONS]({ commit }) {
    ApiService.setHeader();
    return ApiService
      .get('control/sessions');
  },
  [DELETE_SESSION]({ commit }, data) {
    ApiService.setHeader();
    return ApiService.post('control/deletesession', data);
  },
  [EXPORT_SESSION]({ commit }, data) {
    ApiService.setHeader();
    return ApiService.post('control/export', data);
  },
  [EXPORT_SESSIONS]({ commit }) {
    ApiService.setHeader();
    return ApiService.single('export/all');
  }
};

export default {
  state,
  actions,
  getters,
  mutations
};
