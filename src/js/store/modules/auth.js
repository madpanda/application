import { LOGIN, REGISTER, LOGOUT, SET_AUTH, PURGE_AUTH, GET_CURRENT_USER, IS_AUTHENTICATED } from '../mutation-types';
import ApiService from '../../services/Api';
import JwtService from '../../services/Jwt';

const state = {
  isAuthenticated: !!JwtService.getToken()
};

const getters = {
  [GET_CURRENT_USER](state) {
    return state.user;
  },
  [IS_AUTHENTICATED](state) {
    return state.isAuthenticated;
  }
};

const actions = {
  [LOGIN]({ commit }, credentials) {
    return ApiService.post('login', credentials);
  },
  [LOGOUT]({ commit }) {
    ApiService.setHeader();
    return ApiService.single('logout');
  }
};

const mutations = {
  [SET_AUTH](state, config) {
    state.isAuthenticated = true;
    JwtService.save(config);
  },
  [PURGE_AUTH](state) {
    state.isAuthenticated = false;
    state.user = {};
    JwtService.destroy();
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
