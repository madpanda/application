import {
  CHANGE_SCANNER_STATE,
  INITIALIZE_APP,
  IS_INITIALIZED,
  IS_APP_INITIALIZED,
  SCANNER_STATE_CHANGED,
  SETUP_FINISHED,
  SCANNER_STATE, GET_SCANNER_STATE,
  CARD_WRITTEN
} from '../mutation-types';
import ApiService from '../../services/Api';
import JwtService from '../../services/Jwt';

const state = {
  initialized: !!JwtService.isInitialized(),
  state: 3,
  cardWritten: false
};

const mutations = {
  [IS_INITIALIZED](state, config) {
    state.initialized = true;
  },
  [SCANNER_STATE_CHANGED](state, config) {
    state.state = config;
  },
  [SETUP_FINISHED](state, config) {
    state.initialized = config;
  },
  [CARD_WRITTEN](state, config) {
    state.cardWritten = config;
  }
};

const getters = {
  [SCANNER_STATE](state) {
    return state.state;
  }
};

const actions = {
  [IS_APP_INITIALIZED]({ commit }) {
    return ApiService.single('initialized').then((response) => {
      let bool = (response.data.Data === true);
      if (bool) {
        commit(IS_INITIALIZED);
        JwtService.saveItem('initialized', true);
      }
    });
  },
  [GET_SCANNER_STATE]({ commit }) {
    ApiService.setHeader();
    return ApiService.single('/control/getsystemstate').then((response) => {
      if (parseInt(response.data.Code) !== 0) {
        alert('error get state');
        return;
      }

      commit(SCANNER_STATE_CHANGED, response.data.State);
    });
  },
  [INITIALIZE_APP]({ commit }, data) {
    return ApiService.post('register', data);
  },
  [CHANGE_SCANNER_STATE]({ commit }, data) {
    ApiService.setHeader();
    return ApiService
      .post('control/setsystemstate', data);
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
