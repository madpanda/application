const initialized = (state) => state.app.initialized;
const currentSession = (state) => state.sessions.currentSession;
const authenticated = (state) => state.auth.isAuthenticated;
const cardWritten = (state) => state.app.cardWritten;

export {
  currentSession,
  initialized,
  authenticated,
  cardWritten
};
