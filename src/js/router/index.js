import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
  // mode: 'history',
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      redirect: { name: 'login' }
    },
    {
      path: '/login',
      name: 'login',
      meta: {
        auth: false
      },
      component: require('../pages/Login.vue')
    },
    {
      path: '/setup',
      name: 'setup',
      component: require('../pages/Setup.vue')
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: require('../pages/Dashboard.vue')
    },
    {
      path: '/sessions',
      name: 'sessions',
      component: require('../pages/Sessions.vue')
    },
    {
      path: '/shutdown',
      name: 'shutdown',
      component: require('../pages/Shutdown.vue')
    }
  ]
});

export default router;
