require('./core/bootstrap');

import Vue from 'vue';

import { sync } from 'vuex-router-sync';

import router from './router';
import store from './store';

import { IS_APP_INITIALIZED } from './store/mutation-types';

import ApiService from './services/Api';
import JwtService from './services/Jwt';

import moment from 'moment';

import App from './App.vue';

ApiService.init();

sync(store, router);

Vue.filter('formatDate', function(value) {
  return moment(value).format('YYYY-MM-DD h:mm');
});

router.beforeEach((to, from, next) => {
  let getters = store.getters;

  if (!getters.initialized) {
    JwtService.destroy();
    store.dispatch(IS_APP_INITIALIZED).then(() => {
      if (getters.initialized === true) {
        router.push({
          path: 'login'
        });
      } else {
        router.push({
          path: 'setup'
        });
      }

      next();
    });
  }

  if (to.name === 'setup' && getters.initialized === true) {
    router.push({
      path: 'login'
    });

    next();
    return;
  }

  if (to.name === 'setup' && getters.initialized !== true) {
    next();

    return;
  }

  if (to.name !== 'setup' && getters.initialized !== true) {
    router.push({
      path: 'setup'
    });
    next();

    return;
  }

  if (to.name === 'login' && getters.authenticated) {
    router.push({
      path: 'sessions'
    });

    next();
    return;
  }

  next();
});

const app = new Vue({
  router,
  store,
  ...App
});

export { app, store, router };
