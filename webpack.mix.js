/* eslint-disable quote-props */
let mix = require('laravel-mix');

mix.autoload({
  jquery: [
    '$',
    'window.jQuery',
    'jQuery',
    'window.$',
    'jquery',
    'window.jquery'],
  'popper.js/dist/umd/popper.js': ['Popper']
}).js('src/js/index.js', 'public/js').sass('src/sass/app.scss', 'public/css').options({
  processCssUrls: false,
  publicPath: 'public'
}).sourceMaps(false);

// if (process.env.NODE_ENV === 'development') {
//   mix.browserSync('https://interface.dev');
// }
//
